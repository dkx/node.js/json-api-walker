import {expect} from 'chai';

import {DocumentWalker} from '../../src';
import {JsonApiDocument, JsonApiResource} from '@dkx/types-json-api';


describe('#DocumentWalker', () => {

	describe('getDocument()', () => {

		it('should return the document', () => {
			const doc: JsonApiDocument = {};
			const walker = new DocumentWalker(doc);
			expect(walker.getDocument()).to.be.equal(doc);
		});

	});

	describe('isEmpty()', () => {

		it('should return true', () => {
			const walker = new DocumentWalker({});
			expect(walker.isEmpty()).to.be.equal(true);
		});

		it('should return false', () => {
			const walker = new DocumentWalker({
				data: null,
			});

			expect(walker.isEmpty()).to.be.equal(false);
		});

	});

	describe('isNull()', () => {

		it('should return true', () => {
			const walker = new DocumentWalker({
				data: null,
			});

			expect(walker.isNull()).to.be.equal(true);
		});

		it('should return false for item', () => {
			const walker = new DocumentWalker({
				data: {
					type: 'test',
					id: '1',
				},
			});

			expect(walker.isNull()).to.be.equal(false);
		});

		it('should return false for collection', () => {
			const walker = new DocumentWalker({
				data: [],
			});

			expect(walker.isNull()).to.be.equal(false);
		});

	});

	describe('isItem()', () => {

		it('should return true', () => {
			const walker = new DocumentWalker({
				data: {
					type: 'test',
					id: '1',
				},
			});

			expect(walker.isItem()).to.be.equal(true);
		});

		it('should return false for collection', () => {
			const walker = new DocumentWalker({
				data: [],
			});

			expect(walker.isItem()).to.be.equal(false);
		});

		it('should return false for null', () => {
			const walker = new DocumentWalker({
				data: null,
			});

			expect(walker.isItem()).to.be.equal(false);
		});

		it('should return false when empty', () => {
			const walker = new DocumentWalker({});
			expect(walker.isItem()).to.be.equal(false);
		});

	});

	describe('isCollection()', () => {

		it('should return true', () => {
			const walker = new DocumentWalker({
				data: [],
			});

			expect(walker.isCollection()).to.be.equal(true);
		});

		it('should return false', () => {
			const walker = new DocumentWalker({});
			expect(walker.isCollection()).to.be.equal(false);
		});

	});

	describe('getItem()', () => {

		it('should throw an error for non items', () => {
			const walker = new DocumentWalker({});

			expect(() => {
				walker.getItem();
			}).to.throw(Error, 'DocumentWalker: current data is not a resource item');
		});

		it('should return ResourceWalker', () => {
			const walker = new DocumentWalker({
				data: {
					type: 'test',
					id: '1',
				},
			});

			const res = walker.getItem();

			expect(res).to.be.eql({
				type: 'test',
				id: '1',
			});
		});

	});

	describe('getCollection()', () => {

		it('should throw an error if data is a collection', () => {
			const walker = new DocumentWalker({
				data: {
					type: 'test',
					id: '1',
				},
			});

			expect(() => {
				walker.getCollection();
			}).to.throw(Error, 'DocumentWalker: current data is not a resource collection');
		});

		it('should return a collection', () => {
			const walker = new DocumentWalker({
				data: [],
			});

			expect(walker.getCollection()).to.be.eql([]);
		});

	});

	describe('getIncludedRelationship()', () => {

		it('should throw an error when data is missing', () => {
			const walker = new DocumentWalker({});

			expect(() => {
				walker.getIncludedRelationship({});
			}).to.throw(Error, 'DocumentWalker: relationship does not contain any data');
		});

		it('should throw an error when there is no included data', () => {
			const walker = new DocumentWalker({});

			expect(() => {
				walker.getIncludedRelationship({
					data: {
						type: 'test',
						id: '1',
					},
				});
			}).to.throw(Error, 'DocumentWalker: current document is missing the included data');
		});

		it('should throw an error when included data is missing', () => {
			const walker = new DocumentWalker({
				included: [],
			});

			expect(() => {
				walker.getIncludedRelationship({
					data: {
						type: 'test',
						id: '1',
					},
				});
			}).to.throw(Error, 'DocumentWalker: included data for "test/1" does not exists');
		});

		it('should return null', () => {
			const walker = new DocumentWalker({
				included: [],
			});

			const incl = walker.getIncludedRelationship({
				data: null,
			});

			expect(incl).to.be.eql(null);
		});

		it('should return included item', () => {
			const res: JsonApiResource = {
				type: 'test',
				id: '1',
			};

			const walker = new DocumentWalker({
				included: [res],
			});

			const incl = walker.getIncludedRelationship({
				data: res,
			});

			expect(incl).to.be.eql(res);
		});

		it('should return included collection', () => {
			const res: JsonApiResource = {
				type: 'test',
				id: '1',
			};

			const walker = new DocumentWalker({
				included: [res],
			});

			const incl = walker.getIncludedRelationship({
				data: [res],
			});

			expect(incl).to.be.eql([res]);
		});

	});

});

import {JsonApiDocument, JsonApiRelationship, JsonApiResource, JsonApiResourceIdentifier} from '@dkx/types-json-api';


export class DocumentWalker
{


	constructor(
		private document: JsonApiDocument,
	) {}


	public getDocument(): JsonApiDocument
	{
		return this.document;
	}


	public isEmpty(): boolean
	{
		return typeof this.document.data === 'undefined';
	}


	public isNull(): boolean
	{
		return this.document.data === null;
	}


	public isItem(): boolean
	{
		return Object.prototype.toString.call(this.document.data) === '[object Object]';
	}


	public isCollection(): boolean
	{
		return Array.isArray(this.document.data);
	}


	public getItem(): JsonApiResource
	{
		if (!this.isItem()) {
			throw new Error(`DocumentWalker: current data is not a resource item`);
		}

		return <JsonApiResource>this.document.data;
	}


	public getCollection(): Array<JsonApiResource>
	{
		if (!this.isCollection()) {
			throw new Error(`DocumentWalker: current data is not a resource collection`);
		}

		return <Array<JsonApiResource>>this.document.data;
	}


	public getIncludedRelationship(relationship: JsonApiRelationship): null|JsonApiResource|Array<JsonApiResource>
	{
		const relData = relationship.data;

		if (typeof relData === 'undefined') {
			throw new Error(`DocumentWalker: relationship does not contain any data`);
		}

		if (relData === null) {
			return null;
		}

		if (Array.isArray(relData)) {
			return relData.map((identifier) => {
				return this.getIncludedResource(identifier);
			});
		}

		return this.getIncludedResource(<JsonApiResourceIdentifier>relData);
	}


	private getIncludedResource(identifier: JsonApiResourceIdentifier): JsonApiResource
	{
		const included = this.document.included;

		if (typeof included === 'undefined') {
			throw new Error(`DocumentWalker: current document is missing the included data`);
		}

		const resource = included.find((item) => {
			return item.type === identifier.type && item.id === identifier.id;
		});

		if (typeof resource === 'undefined') {
			throw new Error(`DocumentWalker: included data for "${identifier.type}/${identifier.id}" does not exists`);
		}

		return resource;
	}

}

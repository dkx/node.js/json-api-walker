# DKX/JsonApiWalker

Walker utility for JSON API specification.

## Installation

```bash
$ npm install --save @dkx/json-api-walker
```
